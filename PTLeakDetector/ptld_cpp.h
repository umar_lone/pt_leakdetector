#ifndef _PTLD_CPP_H
#define _PTLD_CPP_H

void *operator new (size_t size, const char *pFunction, const char *file,int line)noexcept ;
void operator delete(void *p)noexcept ;

void *operator new[] (size_t size, const char *pFunction, const char *file,int line)noexcept ;
void operator delete[](void *p)noexcept ;

#endif //_PTLD_CPP_H

