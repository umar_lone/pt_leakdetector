#include <malloc.h>
#include "ptmain.h"
#include <string.h>
#include <crtdbg.h>

void ShowLeaksAtExitAndCheckHeapAtFree() {
	puts("Dumping info at exit\n");
	struct PtFlags flags;
	memset(&flags, 0, sizeof(flags)) ;
	flags.checkHeapOnFree = 0 ;
	flags.dumpLeaksOnExit = 1 ;
	
	PtSetFlags(&flags);

	char *p1 = DEBUG_MALLOC(5);
	strcpy(p1, "Hello");
	p1[-1] = 'X';
	float *p3 = (float*)DEBUG_MALLOC(sizeof(float));
	*p3 = 7.2f;
	double *p4 = (double*)DEBUG_MALLOC(sizeof(double));
	*p4 = 8.9;

	//The heap corruption info will be dumped on every delete call
	free(p3);
	free(p4);

}
int main() {
	ShowLeaksAtExitAndCheckHeapAtFree() ;

	puts("End of main");
	_CrtDumpMemoryLeaks() ;

}
