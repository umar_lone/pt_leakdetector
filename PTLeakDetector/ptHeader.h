#ifndef _PTHEADER_H
#define _PTHEADER_H

#ifndef PTBUILD
#error "This header is internally included by ptLeakDetector. Applications should not include this header directly."
#endif

#define GUARDSIZE 4
#include <stddef.h>
struct ptMemoryBlockHeader {
	const char *m_pFunction;
	const char *m_pFile;
	size_t m_Size;
	struct ptMemoryBlockHeader *m_pNext;
	struct ptMemoryBlockHeader *m_pPrev;
	int m_LineNumber;
	//This is the first guard area
	unsigned char m_Guard[GUARDSIZE];
	/*
	 * It will be followed by
	 * unsigned char user_data[size] ;
	 * unsigned char endGuard[GUARDSIZE] ;
	 *
	 */
};
#endif //_PTHEADER_H