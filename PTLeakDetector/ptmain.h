#ifndef _PTMAIN_H
#define _PTMAIN_H
#include "ptld_c.h"
#include <stdio.h>
#ifdef __cplusplus
#include "ptld_cpp.h"
extern "C" {
#endif


struct PtFlags {
	FILE *pStream ;				/*User-specified file pointer										*/
	void (*pFnCloseFile)(void) ;	/*User-specified callback function to close the file				*/
	int dumpLeaksOnExit ;		/*Non-zero value will dump the leaks automatically at exit			*/
	int checkHeapOnFree ;		/*Non-zero value will check heap consistency on every release call	*/
};
void PtSetFlags(struct PtFlags *pFlags) ;
void PtDumpLeaks(void) ;
void PtDumpHeapInfo() ;


#ifdef __cplusplus
}
#endif

#ifdef _DEBUG
	#if defined (_WIN32)
		#define PT_FUNCTIONNAME __FUNCSIG__
	#elif defined (__GNUC__)
		#define PT_FUNCTIONNAME __PRETTY_FUNCTION__
	#else
		#define PT_FUNCTIONNAME "Unknown function"
	#endif /*_WIN32*/

	#define DEBUG_NEW new(PT_FUNCTIONNAME, __FILE__, __LINE__)
	#define DEBUG_MALLOC(size) ptmalloc(size, PT_FUNCTIONNAME, __FILE__, __LINE__)
	#define free ptfree

#else /*Not debug mode*/
	/*Revert to original C/C++ routines*/
	#define DEBUG_NEW new
	#define DEBUG_MALLOC malloc
	/*Remove the calls to these functions in release build*/
	#define PtDumpLeaks()				((void)0)
	#define PtDumpHeapInfo()			((void)0)

#endif /*_DEBUG*/


#endif /*_PTMAIN_H*/

