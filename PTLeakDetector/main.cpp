#include <malloc.h>
#include "ptmain.h"
#include <crtdbg.h>
#include <windows.h>
#include <iostream>
#include "ptld_cpp.h"
void ShowDefault() {
	std::cout << "Dumping info with default options\n";
	char *p1 = DEBUG_NEW char[5]{ };
	strcpy(p1, "Hello");
	p1[-1] = 'X';
	int *p2 = DEBUG_NEW int{ 3 };
	float *p3 = (float*)DEBUG_MALLOC(sizeof(float));
	*p3 = 7.2f;
	double *p4 = (double*)DEBUG_MALLOC(sizeof(double));
	*p4 = 8.9;
	PtDumpLeaks();
	PtDumpHeapInfo();
}
void ShowLeaksAtExit() {
	std::cout << "Dumping info at exit\n";
	PtFlags flags{ nullptr, nullptr, true, false };
	PtSetFlags(&flags);

	char *p1 = DEBUG_NEW char[5]{ };
	strcpy(p1, "Hello");
	p1[-1] = 'X';
	int *p2 = DEBUG_NEW int{ 3 };
	float *p3 = (float*)DEBUG_MALLOC(sizeof(float));
	*p3 = 7.2f;
	double *p4 = (double*)DEBUG_MALLOC(sizeof(double));
	*p4 = 8.9;
	PtDumpHeapInfo() ;
	free(p3) ;
	free(p4) ;
	delete p2 ;
	delete p1 ;


}
void ShowLeaksAtExitAndCheckHeapAtFree() {
	std::cout << "Dumping info at exit\n";
	PtFlags flags{ nullptr, nullptr, true, true };
	PtSetFlags(&flags);

	char *p1 = DEBUG_NEW char[5]{ };
	strcpy(p1, "Hello");
	p1[-1] = 'X';
	int *p2 = DEBUG_NEW int{ 3 };
	float *p3 = (float*)DEBUG_MALLOC(sizeof(float));
	*p3 = 7.2f;
	double *p4 = (double*)DEBUG_MALLOC(sizeof(double));
	*p4 = 8.9;

	//The heap corruption info will be dumped on every delete call
	delete p2;
	free(p3);
	free(p4);

}
void ShowLeaksAtExitInFile() {
	std::cout << "Dumping info at exit with file option\n";
	static FILE *fp = fopen("leaks.log", "w");
	if (!fp) {
		std::cout << "File could not be opened\n";
		return;
	}
	PtFlags flags{ fp, nullptr, true, false };
	PtSetFlags(&flags);

	char *p1 = DEBUG_NEW char[5]{ };
	strcpy(p1, "Hello");
	p1[-1] = 'X';
	int *p2 = DEBUG_NEW int{ 3 };
	float *p3 = (float*)DEBUG_MALLOC(sizeof(float));
	*p3 = 7.2f;
	double *p4 = (double*)DEBUG_MALLOC(sizeof(double));
	*p4 = 8.9;

}
void ShowLeaksAtExitAutoCloseFile() {
	std::cout << "Dumping info with file option and auto close file\n";
	static FILE *fp = fopen("leaks.log", "w");
	if (!fp) {
		std::cout << "File could not be opened\n";
		return;
	}
	PtFlags flags{ fp, []() {fclose(fp); }, true, false };
	PtSetFlags(&flags);

	char *p1 = DEBUG_NEW char[5]{ };
	strcpy(p1, "Hello");
	p1[-1] = 'X';
	int *p2 = DEBUG_NEW int{ 3 };
	float *p3 = (float*)DEBUG_MALLOC(sizeof(float));
	*p3 = 7.2f;
	double *p4 = (double*)DEBUG_MALLOC(sizeof(double));
	*p4 = 8.9;
}

int main() {
	ShowDefault() ;
	//ShowLeaksAtExit() ;
	//ShowLeaksAtExitInFile() ;
	//ShowLeaksAtExitAutoCloseFile() ;
	//ShowLeaksAtExitAndCheckHeapAtFree();
	std::cout << "End of main" << std::endl;

}
