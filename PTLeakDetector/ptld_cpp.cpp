#include <malloc.h>
#include <cstring>
#include <iostream>
#include <cstddef>
#include <algorithm>
#include <cctype>

#include "ptld_cpp.h"
#include "ptld_c.h"
#define PTBUILD
#include "ptHeader.h"
#include <cassert>
#include "ptmain.h"

void* operator new(size_t size, const char* pFunction, const char* file, int line) noexcept {
	//TODO:Think about using thread locks???
	return ptmalloc(size, pFunction, file, line);
}

void operator delete(void*p) noexcept {
	ptfree(p);
}

void* operator new[](size_t size, const char* pFunction, const char* file, int line) noexcept {
	return ptmalloc(size, pFunction, file, line);
}
void operator delete[](void*p) noexcept {
	ptfree(p);
}

