#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include "ptld_c.h"
#define PTBUILD
#include "ptHeader.h"

#include "ptmain.h"
#include <assert.h>

struct ptMemoryBlockHeader *pHead = NULL;



#undef PtDumpLeaks
#undef PtDumpHeapInfo
#undef free

static unsigned const char GUARDFILL = 0XAA;
#define MAXPRINTSIZE 16 

struct PtFlags g_Flags = { NULL };

//G++ prints 0x automatically using p format specifier, but windows does not
#ifdef _WIN32
const char *pFormatString = "Address <0X%p>\n";
#elif defined (__GNUC__)
const char *pFormatString = "Address <%p>\n";
#endif
#pragma region Helpers
size_t ptGetBlockSize(size_t size) {
	return sizeof(struct ptMemoryBlockHeader) + size + GUARDSIZE;
}
struct ptMemoryBlockHeader * ptHeaderFromUserBlock(void *p) {
	unsigned char *user_mem = (unsigned char*)(p);
	return (struct ptMemoryBlockHeader*)(user_mem - sizeof(struct ptMemoryBlockHeader));
}
void * ptUserBlockFromHeader(struct ptMemoryBlockHeader *p) {
	return p + 1;
}
void *ptGetBegGuard(struct ptMemoryBlockHeader *p) {
	return &p->m_Guard;
}
void *ptGetEndGuard(struct ptMemoryBlockHeader *p) {
	unsigned char *user_mem = (unsigned char*)(p);
	return user_mem + sizeof(struct ptMemoryBlockHeader) + p->m_Size;
}
void ptInitMemoryBlock(size_t size, void *p, const char* pFunction, const char* pFile, int line) {
	struct ptMemoryBlockHeader *pCurrentBlock = (struct ptMemoryBlockHeader*)p;
	pCurrentBlock->m_Size = size;
	pCurrentBlock->m_pFunction = pFunction;
	pCurrentBlock->m_pFile = pFile;
	pCurrentBlock->m_LineNumber = line;
	pCurrentBlock->m_pNext = pCurrentBlock->m_pPrev = NULL;
	if (pHead == NULL) {
		pHead = pCurrentBlock;
	} else {
		pCurrentBlock->m_pNext = pHead;
		pHead->m_pPrev = pCurrentBlock;
		pHead = pCurrentBlock;
	}
	memset(pCurrentBlock->m_Guard, GUARDFILL, GUARDSIZE);
	unsigned char *pBlock = (unsigned char*)(pCurrentBlock);
	memset(pBlock + sizeof(struct ptMemoryBlockHeader) + size, GUARDFILL, GUARDSIZE);
}
int ptIsValidBlock(struct ptMemoryBlockHeader *p) {
	if (p == pHead)
		return 1;
	for (struct ptMemoryBlockHeader* ptr = pHead->m_pNext; ptr != NULL; ptr = ptr->m_pNext) {
		if (ptr == p)
			return 1;
	}
	return 0;
}
void ptDumpHelper(struct ptMemoryBlockHeader *ptr) {
	fprintf(g_Flags.pStream, pFormatString, (ptUserBlockFromHeader(ptr)));
	fprintf(g_Flags.pStream, "Line# <%u>\n", ptr->m_LineNumber);
	fprintf(g_Flags.pStream, "Function <%s>\n", ptr->m_pFunction);
	fprintf(g_Flags.pStream, "File <%s>\n", ptr->m_pFile);
}
void ptDumpHeapCorruptionInfo(struct ptMemoryBlockHeader *ptr) {
	/*
 * Workaround for lack of struct initialization in C with non-constant values
 */
	if (g_Flags.pStream == NULL) {
		g_Flags.pStream = stdout;
	}unsigned char guard[4] = { 0 };
	memset(guard, 0xaa, 4);
	if (memcmp(ptGetBegGuard(ptr), guard, GUARDSIZE) != 0) {
		fprintf(g_Flags.pStream, "\nHeap corrupted before memory block\n");
		ptDumpHelper(ptr);
	}
	if (memcmp(ptGetEndGuard(ptr), guard, GUARDSIZE) != 0) {
		fprintf(g_Flags.pStream, "\nHeap corrupted after memory block\n");
		ptDumpHelper(ptr);
	}
}
void ptDumpLeakInfo(struct ptMemoryBlockHeader *ptr) {
	/*
	 * Workaround for lack of struct initialization in C with non-constant values
	 */
	if (g_Flags.pStream == NULL) {
		g_Flags.pStream = stdout;
	}
	fprintf(g_Flags.pStream, "\nBytes leaked <%zu>\n", ptr->m_Size);
	fprintf(g_Flags.pStream, pFormatString, ptUserBlockFromHeader(ptr));

	char dataBuffer[MAXPRINTSIZE + 1] = { '\0' };		//+1 is for null
	char valueBuffer[MAXPRINTSIZE * 3 + 1] = { '\0' };	//*3 is required because each hex value will be 2 characters plus space (total 3)
	unsigned char *user_mem = (unsigned char*)(ptUserBlockFromHeader(ptr));
	for (unsigned int i = 0; i < min(ptr->m_Size, MAXPRINTSIZE); ++i) {
		unsigned char ch = *(user_mem + i);
		unsigned char ch2 = ch;
		if (isalpha(ch) == 0 && ispunct(ch) == 0) {
			ch2 = ' ';
		}
		sprintf(dataBuffer + strlen(dataBuffer), "%c", ch2);
		sprintf(valueBuffer + strlen(valueBuffer), "%.2X ", ch);
	}
	fprintf(g_Flags.pStream, "Data <%s> <%s>\n", dataBuffer, valueBuffer);
	fprintf(g_Flags.pStream, "Line# <%d>\n", ptr->m_LineNumber);
	fprintf(g_Flags.pStream, "Function <%s>\n", ptr->m_pFunction);
	fprintf(g_Flags.pStream, "File <%s>\n", ptr->m_pFile);

}

void * ptNew(size_t size, const char* pFunction, const char* file, int line) {
	void *allocated_mem = malloc(ptGetBlockSize(size));
	if (!allocated_mem)
		return NULL;
	ptInitMemoryBlock(size, allocated_mem, pFunction, file, line);
	return ptUserBlockFromHeader((struct ptMemoryBlockHeader*)(allocated_mem));
}
void ptDelete(void *p) {
	if (g_Flags.checkHeapOnFree)
		PtDumpHeapInfo();
	struct ptMemoryBlockHeader *mem_block = ptHeaderFromUserBlock(p);
	//Check if the block is from our list
	if (ptIsValidBlock(mem_block)) {
		if (mem_block->m_pPrev != NULL) {
			mem_block->m_pPrev->m_pNext = mem_block->m_pNext;
		} else {
			pHead = mem_block->m_pNext;
		}
		if (mem_block->m_pNext != NULL) {
			mem_block->m_pNext->m_pPrev = mem_block->m_pPrev;
		}
		free(mem_block);
	} else {//Not our block, so ignore it
		//free(p) ;
	}
}
#pragma endregion 

#pragma region APIS
void PtDumpLeaks(void) {
	/*
 * Workaround for lack of struct initialization in C with non-constant values
 */
	if (g_Flags.pStream == NULL) {
		g_Flags.pStream = stdout;
	}
	if (pHead == NULL) {
		fprintf(g_Flags.pStream, "No leaks detected\n");
		return;
	}

	fprintf(g_Flags.pStream, "\n--------------\n");
	fprintf(g_Flags.pStream, "LEAKS DETECTED\n");
	fprintf(g_Flags.pStream, "--------------\n");
	for (struct ptMemoryBlockHeader * ptr = pHead; ptr != NULL; ptr = ptr->m_pNext) {
		ptDumpLeakInfo(ptr);
	}
}

void PtDumpHeapInfo() {
	//if(pHead == NULL)
		//return ;
	for (struct ptMemoryBlockHeader * ptr = pHead; ptr != NULL; ptr = ptr->m_pNext) {
		ptDumpHeapCorruptionInfo(ptr);
	}
}

void PtSetFlags(struct PtFlags *pFlags) {
	assert(pFlags != NULL);

	g_Flags.dumpLeaksOnExit = pFlags->dumpLeaksOnExit;
	g_Flags.pStream = pFlags->pStream;
	g_Flags.pFnCloseFile = pFlags->pFnCloseFile;
	g_Flags.checkHeapOnFree = pFlags->checkHeapOnFree;

	/*
	 * Registered functions are called in reverse order.
	 * So, ensure we register the callback for closing the file first
	 * so that the runtime can it after dumping the leaks.
	 * This is applicable only if the user has specified a callback
	 */
	if (g_Flags.pFnCloseFile != NULL)
		atexit(g_Flags.pFnCloseFile);
	if (g_Flags.dumpLeaksOnExit)
		atexit(PtDumpLeaks);
}

#pragma endregion


void* ptmalloc(size_t size, const char* pFunction, const char* file, int line) {
	return ptNew(size, pFunction, file, line);
}

void ptfree(void* p) {
	/*
	 * If our list is empty and p is a valid pointer, it was not
	 * allocated by us, so ignore.
	 */
	if (p == NULL || pHead == NULL)
		return;
	ptDelete(p);
}