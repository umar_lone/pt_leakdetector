#ifndef _PTLD_C_H
#define _PTLD_C_H

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * GCC uses empty throw specification on C functions when compiled in C++ mode,
 * and  __THROW macro requires sys/cdefs.h header
 */
#ifdef __GNUC__
#include <sys/cdefs.h>
#else
#define __THROW
#endif

void *ptmalloc(size_t size, const char *pFunction, const char *file,int line)__THROW ;
void ptfree(void *p)__THROW ;


#ifdef __cplusplus
}
#endif

#endif //_PTLD_C_H
